import os
import pandas as pd
import streamlit as st
import psycopg2

columns = [
    "uid", "Номер квартиры", "Стена штукатурка", "Стена чистовая", 
    "Пол чистовая", "Пол стяжка", "Потолок чистовая", 
    "Пол без отделки", "Потолок без отделки", "Стена без отделки",
    "Отделка окна", "Есть двери", "Есть унитаз", "Есть ванная", "Есть кухня", "Есть строительный мусор"
]


def get_connector():
    HOST = 'rc1b-hinbi4qh71yvsof6.mdb.yandexcloud.net'
    conn = psycopg2.connect(
        user='user1', password='passpass', 
        host=HOST, port=6432, dbname='hack'
    )
    return conn

st.title("Шахматка")

conn = get_connector()
cur = conn.cursor()
with conn.cursor() as cur:
    cur.execute('SELECT * FROM public.construct_statuses')
    all_data = cur.fetchall()

df = pd.DataFrame(
    all_data,
    columns=columns
)

st.dataframe(df)
