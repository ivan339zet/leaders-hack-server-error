# OCR Server

## Run
Simplest way to run is Docker:
```bash 
docker build -t ocr-server .
docker run --network host ocr-server --port <port>
```

To infer model send POST request:
```python
import requests
import base64

with open(image_path, "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read()).decode('utf-8')

r = requests.post("<url>:<port>/recognize_nameplate", json={"image": encoded_string})

print(r.json())

### 
# Out: {'result': '250'}
###
```
