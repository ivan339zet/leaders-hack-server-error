import io
import re
import base64
import numpy as np
from PIL import Image
from paddleocr import PaddleOCR
from flask import Flask, request, jsonify

app = Flask(__name__)

ocr = PaddleOCR(use_angle_cls=True, lang='en', show_log=False)

@app.route("/recognize_nameplate", methods=["POST"])
def recognize_nameplate():
    content = request.json
    img_data = base64.b64decode(content["image"])
    image = Image.open(io.BytesIO(img_data))
    image_np = np.array(image)[:,:,:3]
    result = ocr.ocr(image_np, cls=True)
    line = ""
    if len(result[0]) > 0:
        threshold = result[0][0][-1][1]
        if threshold > 0.85:
            line = result[0][0][-1][0]
            line = re.sub("\D", "", line)
    return jsonify({"result": line})
